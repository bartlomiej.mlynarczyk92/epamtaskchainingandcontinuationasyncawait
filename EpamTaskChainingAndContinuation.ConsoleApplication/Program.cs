﻿using EpamTaskChainingAndContinuation.Library;
class Program
{
    static async Task Main()
    {

        TaskChainingAndContinuation tasks = new TaskChainingAndContinuation();

        Random r = new Random();
        var array = await tasks.CreateArray(10);
        arrayPrinter<int>("created array:", array);
        var initializedArray = await tasks.InitializeWithRandom(array);
        arrayPrinter<int>("initialized array:", initializedArray);
        var multipliedArray = await tasks.multiplyArray(initializedArray, r.Next());
        arrayPrinter<long>("multiplied array:", multipliedArray);
        var sortedArray = await tasks.sortArrayAscending(multipliedArray);
        arrayPrinter<long>("sorted array:", sortedArray);
        var average = await tasks.calculateAverage(sortedArray);

        Console.WriteLine($"average : {average}");
        Console.ReadLine();
    }

    private static void arrayPrinter<T>(string header, T[] array) where T : struct
    {
        if (array == null)
        {
            throw new ArgumentNullException(nameof(array));
        }
        Console.WriteLine(header);
        for (int i = 0; i < array.Length; i++)
        {
            Console.WriteLine(array[i].ToString());
        }
        Console.WriteLine();
    }
}
﻿namespace EpamTaskChainingAndContinuation.Library
{
    public class TaskChainingAndContinuation
    {

        //Task 1: creates an array of 10 random integers.
        //Task 2: multiplies the array by a randomly generated number.
        //Task 3: sorts the array by ascending.
        //Task 4: calculates the average value


        public async Task<decimal> calculateAverageWithAsyncAwait(int arraySize =10)
        {
            Random r = new Random();
            var array = await CreateArray(10);
            var initializedArray = await InitializeWithRandom(array);
            var multipliedArray = await multiplyArray(initializedArray, r.Next());
            var sortedArray = await sortArrayAscending(multipliedArray);
            var average = await calculateAverage(sortedArray);
            return average;
        }


        public async Task<int[]> CreateArray(int size = 10)
        {
          return  await Task.Run( () =>
            {
                if (size < 1)
                {
                    throw new ArgumentException($"Array cannot be smaller then 1!");
                }
                return new int[size];
            });       
        }

        public async Task<int[]> InitializeWithRandom(int[] array)
        {

           return await Task.Run(() =>
            {
                validateArray<int[]>(array);
                var r = new Random();
                for (var i = 0; i < array.Length; i++)
                {
                    array[i] = r.Next();
                }
                return array;
            });        
            
        }

        public async Task<long[]> multiplyArray(int[] array, int multiplier)
        {
           return await Task.Run(() =>
            {
                validateArray<int[]>(array);
                var resultArray = new long[array.Length];
                for (var i = 0; i < array.Length; i++)
                {
                    checked
                    {
                        resultArray[i] = array[i] * (long)multiplier;
                    }
                }
                return resultArray;
            });
        }

        public async Task<long[]> sortArrayAscending(long[] array)
        {
            return await Task.Run(() =>
            {
                validateArray<long[]>(array);
                var sortedArray = new long[array.Length];
                Array.Copy(array, 0, sortedArray, 0, array.Length);
                Array.Sort(sortedArray, 0, sortedArray.Length);
                return sortedArray;
            });

        }

        public async Task<decimal> calculateAverage(long[] array)
        {
            return await Task.Run(() =>
            {
                validateArray<long[]>(array);
                var average = Convert.ToDecimal(array[0]);
                for (var i = 1; i < array.Length; i++)
                {
                    checked
                    {
                        average = (average + array[i]) / 2;
                    }
                }
                return average;
            });
        }

        private void validateArray<T>(T array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
        }

    }
}
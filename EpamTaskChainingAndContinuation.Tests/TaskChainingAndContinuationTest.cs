using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EpamTaskChainingAndContinuation.Library;
using NUnit.Framework;

namespace EpamTaskChainingAndContinuation.Tests
{
    /// <summary>
    /// This class tests logic of TaskChainingAndContinuation.
    /// </summary>
    [TestFixture]
    public class TesTaskChainingAndContinuationTest
    {
        TaskChainingAndContinuation tasks;
        Random randomGenerator;

        [SetUp]
        public void setUp()
        {
            tasks = new TaskChainingAndContinuation();
            randomGenerator = new Random();
        }


#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
        [Test]
        public void TestMultiplicationThrowsNullException()
        {

            Assert.ThrowsAsync<ArgumentNullException>(async () => await tasks.multiplyArray(null, 3));

        }

        [Test]
        public void TestInitializationThrowsNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await tasks.InitializeWithRandom(null));
        }

        [Test]
        public void TestSortingThrowsNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await tasks.sortArrayAscending(null));
        }

        [Test]
        public void TestAverageThrowsNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await tasks.calculateAverage(null));
        }
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.

        [Test]
        public void TestCreateArrayThrowsArgumentException()
        {

            Assert.ThrowsAsync<ArgumentException>(async () => await tasks.CreateArray(0));
            Assert.ThrowsAsync<ArgumentException>(async () => await tasks.CreateArray(-1));
            Assert.ThrowsAsync<ArgumentException>(async () => await tasks.CreateArray(int.MinValue));
        }



        [Test]
        public void TestCalculateAverageWithAsyncAwait()
        {
            Assert.DoesNotThrowAsync(() => tasks.calculateAverageWithAsyncAwait(1000));
        }

        [Test]
        public void TestTaskChainingRandom()
        {
            Random r = new Random();
            Assert.DoesNotThrowAsync(async () =>
            {
                var array = await tasks.CreateArray(10);
                var initializedArray = await tasks.InitializeWithRandom(array);
                var multipliedArray = await tasks.multiplyArray(initializedArray, r.Next());
                var sortedArray = await tasks.sortArrayAscending(multipliedArray);
                await tasks.calculateAverage(sortedArray);
            }
           );



        }

        [Test]
        public async Task TestArrayInitialization()
        {
            var array = new int[100];
            var initializedArray = await tasks.InitializeWithRandom(array);
            bool areAllZeros = false;

            for (int i = 0; i < initializedArray.Length; i++)
            {
                if (initializedArray[i] != 0)
                {
                    areAllZeros = false;
                }
            }
            Assert.IsFalse(areAllZeros, $"Very unlucky test or values not initializated");
        }


        [Test]
        public async Task TestPositiveArrayMultiplication()
        {
            int[] array = new int[3];
            array[0] = int.MaxValue;
            array[1] = int.MaxValue;
            array[2] = int.MaxValue;

            var multiplier = int.MaxValue;
            var resultArray = await tasks.multiplyArray(array, multiplier);
            foreach (var result in resultArray)
            {
                Assert.That(result, Is.EqualTo(4611686014132420609));
            }
        }

        [Test]
        public async Task TestNegativeArrayMultiplication()
        {
            int[] array = new int[3];
            array[0] = int.MinValue;
            array[1] = int.MinValue;
            array[2] = int.MinValue;

            var multiplier = int.MinValue;
            var resultArray = await tasks.multiplyArray(array, multiplier);

            foreach (var result in resultArray)
            {
                Assert.That(result, Is.EqualTo(4611686018427387904));
            }
        }

        [Test]
        public async Task TestArrayZeroMultiplication()
        {
            int[] array = new int[3];
            array[0] = int.MinValue;
            array[1] = int.MinValue;
            array[2] = int.MinValue;

            var multiplier = 0;
            var resultArray = await tasks.multiplyArray(array, multiplier);

            foreach (var result in resultArray)
            {
                Assert.That(result, Is.EqualTo(0));
            }
        }

        [Test]
        public void TestArrayRandomMultiplicationDoesNotThrow()
        {
            Assert.DoesNotThrowAsync(async () =>
             {
                 for (int i = 0; i < 20; i++)
                 {
                     int[] array = new int[20];
                     for (int j = 0; j < array.Length; j++)
                     {
                         array[j] = randomGenerator.Next();
                     }
                     var multiplier = randomGenerator.Next();
                     await tasks.multiplyArray(array, multiplier);
                 }
             });
        }



        [Test]
        public async Task TestArraySoringAscending()
        {
            var array = new long[6];
            array[0] = int.MinValue;
            array[1] = int.MaxValue;
            array[2] = 0;
            array[3] = 1;
            array[4] = -1;
            array[5] = 0;

            var sortedArray = new long[6] { int.MinValue, -1, 0, 0, 1, int.MaxValue };
            var sortedByTaskArray = await tasks.sortArrayAscending(array);
            Assert.That(sortedByTaskArray, Is.EqualTo(sortedArray));
        }

        [Test]
        public async Task TestCalculateAverage()
        {
            var array = new long[6];
            array[0] = long.MinValue;
            array[1] = long.MinValue;
            array[2] = long.MinValue;
            array[3] = long.MinValue;
            array[4] = long.MinValue;
            array[5] = long.MinValue;

            var average = await tasks.calculateAverage(array);
            Assert.That(average, Is.EqualTo(long.MinValue));
        }

        [Test]
        public async Task TestCalculateAverageWithFraction()
        {
            var array = new long[2];
            array[0] = long.MaxValue;
            array[1] = long.MaxValue - 1;

            var average = await tasks.calculateAverage(array);
            Assert.That(average, Is.EqualTo(9223372036854775806.5));
        }

        public async Task TestCalculateAverageRandom()
        {
            var array = new long[120];
            for (var i = 0; i < array.Length; i++)
            {
                array[i] = randomGenerator.NextInt64();
            }
            var excepctedAverage = array.AsEnumerable().Average();
            var average = await tasks.calculateAverage(array);
            Assert.That(average, Is.EqualTo(excepctedAverage));
        }
    }
}